<?php

namespace App\Entity;

use App\Repository\PurchasingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PurchasingRepository::class)
 */
class Purchasing
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $totalPrice;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timestamp;

    /**
     * @ORM\Column(type="integer")
     */
    private $month;

    /**
     * @ORM\OneToMany(targetEntity=PurchasingItem::class, mappedBy="purchasing", orphanRemoval=true)
     */
    private $purchasingItems;

    /**
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @ORM\Column(type="integer")
     */
    private $paymentMethod;

//    /**
//     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="purchasings")
//     * @ORM\JoinColumn(nullable=false)
//     */
//    private $person;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $userHash;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $purchasingType;

    public function __construct()
    {
        $this->purchasingItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTotalPrice(): ?float
    {
        return $this->totalPrice;
    }

    public function setTotalPrice(?float $totalPrice): self
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    public function getTimestamp(): ?\DateTimeInterface
    {
        return $this->timestamp;
    }

    public function setTimestamp(\DateTimeInterface $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    public function getMonth(): ?int
    {
        return $this->month;
    }

    public function setMonth(int $month): self
    {
        $this->month = $month;

        return $this;
    }

    /**
     * @return Collection|PurchasingItem[]
     */
    public function getPurchasingItems(): Collection
    {
        return $this->purchasingItems;
    }

    public function addPurchasingItem(PurchasingItem $purchasingItem): self
    {
        if (!$this->purchasingItems->contains($purchasingItem)) {
            $this->purchasingItems[] = $purchasingItem;
            $purchasingItem->setPurchasing($this);
        }

        return $this;
    }

    public function removePurchasingItem(PurchasingItem $purchasingItem): self
    {
        if ($this->purchasingItems->removeElement($purchasingItem)) {
            // set the owning side to null (unless already changed)
            if ($purchasingItem->getPurchasing() === $this) {
                $purchasingItem->setPurchasing(null);
            }
        }

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getPaymentMethod(): ?int
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(int $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

//    public function getPerson(): ?Person
//    {
//        return $this->person;
//    }

//    public function setPerson(?Person $person): self
//    {
//        $this->person = $person;
//
//        return $this;
//    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUserHash(): ?string
    {
        return $this->userHash;
    }

    public function setUserHash(?string $userHash): self
    {
        $this->userHash = $userHash;

        return $this;
    }

    public function getPurchasingType(): ?int
    {
        return $this->purchasingType;
    }

    public function setPurchasingType(?int $purchasingType): self
    {
        $this->purchasingType = $purchasingType;

        return $this;
    }
}
