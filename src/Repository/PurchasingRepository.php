<?php

namespace App\Repository;

use App\Entity\Purchasing;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Purchasing|null find($id, $lockMode = null, $lockVersion = null)
 * @method Purchasing|null findOneBy(array $criteria, array $orderBy = null)
 * @method Purchasing[]    findAll()
 * @method Purchasing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PurchasingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Purchasing::class);
    }

    public function getPurchasingsByUserHash($user)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "
                SELECT p.*, u.first_name, u.sur_name 
                FROM 
                    purchasing p 
                LEFT JOIN user u on u.id = p.user_id 
                WHERE 
                    p.user_hash = '".$user->getShoppingCardHash()."' 
                ORDER BY 
                    p.id DESC
                ";
        $stmt = $conn->prepare($sql);

        return $stmt->executeQuery()->fetchAllAssociative();
    }

    // /**
    //  * @return Purchasing[] Returns an array of Purchasing objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Purchasing
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
