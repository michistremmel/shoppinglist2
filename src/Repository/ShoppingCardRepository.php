<?php

namespace App\Repository;

use App\Entity\ShoppingCard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ShoppingCard|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShoppingCard|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShoppingCard[]    findAll()
 * @method ShoppingCard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShoppingCardRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShoppingCard::class);
    }

    public function getShoppingCardProducts($userHash)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "SELECT sc.id, sc.bought, sc.quantity, pr.name, pr.id AS product_id 
                FROM shopping_card sc 
                LEFT JOIN product pr on pr.id = sc.product_id and pr.deleted IS NULL 
                WHERE sc.user_hash = '".$userHash."'
                ORDER BY pr.name ASC";
        $stmt = $conn->prepare($sql);

        return $stmt->executeQuery()->fetchAllAssociative();

    }

    public function shoppingCardDeleteProducts($userHash)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "DELETE FROM shopping_card WHERE user_hash = '".$userHash."'";
        $stmt = $conn->prepare($sql);

        return $stmt->executeQuery();

    }

    public function shoppingCardDeleteAllBoughtProducts($userHash)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "DELETE FROM shopping_card WHERE user_hash = '".$userHash."' AND bought = 1";
        $stmt = $conn->prepare($sql);

        return $stmt->executeQuery();

    }

    // /**
    //  * @return ShoppingCard[] Returns an array of ShoppingCard objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ShoppingCard
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
