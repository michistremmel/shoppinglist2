<?php

namespace App\Repository;

use App\Entity\PurchasingItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PurchasingItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method PurchasingItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method PurchasingItem[]    findAll()
 * @method PurchasingItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PurchasingItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PurchasingItem::class);
    }

    // /**
    //  * @return PurchasingItem[] Returns an array of PurchasingItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PurchasingItem
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
