<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", options = { "expose" = true }, name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @param SessionInterface $session
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils, SessionInterface $session): Response
    {
        // https://www.youtube.com/watch?v=lk3mBIwJyIk

        if ($this->getUser()){
            return $this->redirectToRoute('indexPage', ['userHash' => $this->getUser()->getShoppingCardHash()]);
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error, 'user_hash' => $session->has('user_hash') ?? $session->get('user_hash')]);
    }

    /**
     * @Route("/logout", name="app_logout")
     * @return Response
     */
    public function logout(): Response
    {
        return $this->redirectToRoute('app_login');
    }

    /**
     * @Route("/register", options = { "expose" = true }, name="app_register")
     * @return Response
     */
    public function register(): Response
    {
        return true;
    }

    /**
     * @Route("/", name="indexPage")
     */
    public function index(): Response
    {
        if ($this->getUser()){
            return $this->redirectToRoute('shopping_list', ['userHash' => $this->getUser()->getShoppingCardHash()]);
        }
        return $this->redirectToRoute('app_login');
    }
}
