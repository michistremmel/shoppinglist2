<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/user/create", name="create_user")
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function create(EntityManagerInterface $entityManager)
    {
        $date = new \DateTime();
        $encodedPassword = password_hash('test4711', PASSWORD_BCRYPT, ['cost' => 12]);
        $userHash = md5($date->getTimestamp());

        $user = new User();
        $user->setEmail('michael.stremmel@googlemail.com');
        $user->setFirstName('Michael');
        $user->setSurName('Stremmel');
        $user->setRoles([]);
        $user->setPassword($encodedPassword);
        $user->setShoppingCardHash($userHash);
        $entityManager->persist($user);
        $entityManager->flush();

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setContent(json_encode(true));

        return $response;
    }
}
