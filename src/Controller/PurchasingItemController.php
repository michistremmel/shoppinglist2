<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PurchasingItemController extends AbstractController
{
    /**
     * @Route("/purchasing/item", name="purchasing_item")
     */
    public function index(): Response
    {
        return $this->render('purchasing_item/index.html.twig', [
            'controller_name' => 'PurchasingItemController',
        ]);
    }
}
