<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\ShoppingCard;
use App\Repository\ShoppingCardRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class ShoppingListController extends AbstractController
{
    /**
     * @Route("/shoppinglist/{userHash}", options = { "expose" = true }, name="shopping_list")
     * @param $userHash
     * @param UserRepository $userRepository
     * @param SessionInterface $session
     * @return Response
     */
    public function shoppingListIndex($userHash, UserRepository $userRepository, SessionInterface $session)
    {
        if(!$userRepository->findOneBy(['shoppingCardHash' => $userHash])){
            return $this->redirectToRoute('app_login');
        }

        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/shoppinglist/{userHash}/products", options = { "expose" = true }, name="shopping_list_products")
     * @param $userHash
     * @param ShoppingCardRepository $shoppingCardRepository
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function shoppingListProducts($userHash, ShoppingCardRepository $shoppingCardRepository, SerializerInterface $serializer)
    {
        $data['shopping_card'] = $shoppingCardRepository->getShoppingCardProducts($userHash);
        $data['user'] = $this->getUser();

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setContent($serializer->serialize($data, 'json'));

        return $response;
    }

    /**
     * @Route("/shoppinglist/{userHash}/products/delete", options = { "expose" = true }, name="shopping_list_delete_products")
     * @param $userHash
     * @param ShoppingCardRepository $shoppingCardRepository
     * @return Response
     */
    public function shoppingListDeleteProducts($userHash, ShoppingCardRepository $shoppingCardRepository)
    {
        $shoppingCard = $shoppingCardRepository->shoppingCardDeleteProducts($userHash);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setContent(json_encode($shoppingCard));

        return $response;
    }

    /**
     * @Route("/shoppinglist/{userHash}/add-product/{id}", options = { "expose" = true }, name="shopping_list_add_product")
     * @param $userHash
     * @param Product $product
     * @param EntityManagerInterface $entityManager
     * @param ShoppingCardRepository $shoppingCardRepository
     * @return Response
     */
    public function addProduct($userHash, Product $product, EntityManagerInterface $entityManager, ShoppingCardRepository $shoppingCardRepository)
    {
        if($product && ($product->getUserHash() == $userHash)){
            $shoppingCard = new ShoppingCard();
            $shoppingCard->setProduct($product);
            $shoppingCard->setBought(null);
            $shoppingCard->setQuantity(1);
            $shoppingCard->setUserHash($userHash);
            $entityManager->persist($shoppingCard);
            $entityManager->flush();
        }

        $shoppingCard = $shoppingCardRepository->getShoppingCardProducts($userHash);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setContent(json_encode($shoppingCard));

        return $response;
    }

    /**
     * @Route("/shoppinglist/{userHash}/{id}/change-bought", options = { "expose" = true }, name="shopping_list_change_bought")
     * @param $userHash
     * @param ShoppingCard $shoppingCard
     * @param EntityManagerInterface $entityManager
     * @param ShoppingCardRepository $shoppingCardRepository
     * @return Response
     */
    public function changeBought($userHash, ShoppingCard $shoppingCard, EntityManagerInterface $entityManager, ShoppingCardRepository $shoppingCardRepository)
    {
        if($shoppingCard && ($shoppingCard->getUserHash() == $userHash)){
            $shoppingCard->setBought($shoppingCard->getBought() ? null : true);
            $entityManager->persist($shoppingCard);
            $entityManager->flush();
        }

        $shoppingCard = $shoppingCardRepository->getShoppingCardProducts($userHash);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setContent(json_encode($shoppingCard));

        return $response;
    }

    /**
     * @Route("/shoppinglist/{userHash}/{id}/change-quantity/{quantity}", options = { "expose" = true }, name="shopping_list_change_quantity")
     * @param $userHash
     * @param $quantity
     * @param ShoppingCard $shoppingCard
     * @param EntityManagerInterface $entityManager
     * @param ShoppingCardRepository $shoppingCardRepository
     * @return Response
     */
    public function changeQuantity($userHash, $quantity, ShoppingCard $shoppingCard, EntityManagerInterface $entityManager, ShoppingCardRepository $shoppingCardRepository)
    {
        if($shoppingCard && ($shoppingCard->getUserHash() == $userHash)){
            $shoppingCard->setQuantity($quantity);
            $entityManager->persist($shoppingCard);
            $entityManager->flush();
        }

        $shoppingCard = $shoppingCardRepository->getShoppingCardProducts($userHash);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setContent(json_encode($shoppingCard));

        return $response;
    }
}
