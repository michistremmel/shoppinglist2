<?php

namespace App\Controller;

use App\Entity\Purchasing;
use App\Entity\PurchasingItem;
use App\Entity\ShoppingCard;
use App\Repository\PersonRepository;
use App\Repository\PurchasingRepository;
use App\Repository\ShoppingCardRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class PurchasingController extends AbstractController
{
    /**
     * @Route("/purchasing/add", options = { "expose" = true }, name="add_purchasing")
     * @param ShoppingCardRepository $shoppingCardRepository
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return Response
     */
    public function add(ShoppingCardRepository $shoppingCardRepository, EntityManagerInterface $entityManager, Request $request)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        if (!$this->isGranted('ROLE_USER')) {
            return $response->setContent(json_encode(false));
        }

        $totalPrice = $request->request->get('total_price') ?? null;

        $userHash = $this->getUser()->getShoppingCardHash();
        $shoppingCardItems = $shoppingCardRepository->findBy(['userHash' => $userHash]);

        if(!$shoppingCardItems || !$totalPrice) {
            return $response->setContent(json_encode(false));

        }

        $date = new \DateTime();

        // Create new Purchasing
        $purchasing = new Purchasing();
        $purchasing->setTimestamp($date);
        $purchasing->setMonth($date->format('m'));
        $purchasing->setYear($date->format('Y'));
        $purchasing->setTotalPrice($totalPrice);
        $purchasing->setPaymentMethod(3);
        $purchasing->setUser($this->getUser());
        $purchasing->setUserHash($userHash);
        $purchasing->setPurchasingType(1); // Lebensmittel

        $entityManager->persist($purchasing);

        $flushPurchasing = false;

        // Loop all Products from ShoppingCard
        foreach ($shoppingCardItems as $shoppingCardItem) {
            if($shoppingCardItem->getBought()){
                $flushPurchasing = true;

                // Create new PurchasingItem
                $purchasingItem = new PurchasingItem();
                $purchasingItem->setPurchasing($purchasing);
                $purchasingItem->setProduct($shoppingCardItem->getProduct());
                $purchasingItem->setOriginalName($shoppingCardItem->getProduct()->getName());
                $purchasingItem->setQuantity($shoppingCardItem->getQuantity());
                $entityManager->persist($purchasingItem);
            }
        }

        if($flushPurchasing){
            // We have added some bought ProductItems and can flush the Purchasing
            $entityManager->flush();

            // Delete all bought Positions on the Shoppinglist
            $shoppingCardRepository->shoppingCardDeleteAllBoughtProducts($userHash);
        }

        // Get rest of ProductItems from the Shoppinglist
        // in this Case we only have Items wich are not bought
        $shoppingCard = $shoppingCardRepository->getShoppingCardProducts($userHash);

        return $response->setContent(json_encode($shoppingCard));
    }

    /**
     * @Route("/purchasing/index", options = { "expose" = true }, name="get_purchasing")
     * @param PurchasingRepository $purchasingRepository
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function getPurchasing(PurchasingRepository $purchasingRepository, SerializerInterface $serializer)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        if (!$this->isGranted('ROLE_USER')) {
            return $response->setContent(json_encode(false));
        }

        // Get all Purchasings by userHash
        $purchasing = $purchasingRepository->getPurchasingsByUserHash($this->getUser());

        // Get all different Users from Purchasing
        $userList = [];
        if($purchasing){
            foreach ($purchasing as $purchase) {
                if(!array_key_exists($purchase['user_id'], $userList)){
                    $userList[$purchase['user_id']] = $purchase['first_name'].' '.$purchase['sur_name'];
                }
            }
        }

        $data['user_list'] = $userList;
        $data['purchasing'] = $purchasing;
        $data['user'] = $this->getUser();

        return $response->setContent($serializer->serialize($data, 'json'));
    }

    /**
     * @Route("/purchasing", options = { "expose" = true }, name="purchasing")
     */
    public function index()
    {
        if (!$this->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('app_login');
        }

        return $this->render('default/index.html.twig');
    }
}
