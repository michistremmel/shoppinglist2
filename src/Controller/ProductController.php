<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/products/{userHash}/index", options = { "expose" = true }, name="products_index")
     * @param $userHash
     * @param ProductRepository $productRepository
     * @return Response
     */
    public function productsIndex($userHash, ProductRepository $productRepository)
    {
        $products = $productRepository->getProducts($userHash);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setContent(json_encode($products));

        return $response;
    }

    /**
     * @Route("/product/{userHash}/add/{name}", options = { "expose" = true }, name="add_product")
     * @param $userHash
     * @param $name
     * @param ProductRepository $productRepository
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function addProduct($userHash, $name, ProductRepository $productRepository, EntityManagerInterface $entityManager)
    {
        if($name){
            $product = new Product();
            $product->setName($name);
            $product->setUserHash($userHash);
            $entityManager->persist($product);
            $entityManager->flush();
        }

        $products = $productRepository->getProducts($userHash);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setContent(json_encode($products));

        return $response;
    }

    /**
     * @Route("/product/{userHash}/update/{id}/{name}", options = { "expose" = true }, name="update_product")
     * @param $userHash
     * @param $id
     * @param $name
     * @param ProductRepository $productRepository
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function updateProduct($userHash, $id, $name, ProductRepository $productRepository, EntityManagerInterface $entityManager)
    {
        $product = [];

        if($id && $name){
            $product = $productRepository->findOneBy(['id' => $id, 'userHash' => $userHash]);
            $product->setName($name);
            $entityManager->persist($product);
            $entityManager->flush();
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setContent(json_encode($product ? $product->getName() : false));

        return $response;
    }

    /**
     * @Route("/product/{userHash}/delete/{id}", options = { "expose" = true }, name="delete_product")
     * @param $userHash
     * @param $id
     * @param ProductRepository $productRepository
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function deleteProduct($userHash, $id, ProductRepository $productRepository, EntityManagerInterface $entityManager)
    {
        $product = [];

        if($id){
            $product = $productRepository->findOneBy(['id' => $id, 'userHash' => $userHash]);
            $product->setDeleted(1);
            $entityManager->persist($product);
            $entityManager->flush();
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->setContent(json_encode(true));

        return $response;
    }
}
