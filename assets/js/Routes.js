import Home from "./pages/Shoppinglist"
import Products from "./pages/Products";

const RoutesArray = [
    {
        path: '/shoppinglist/:userHash',
        element: <Home />,
        isDisplayed: true,
        title: 'Shoppinglist'
    }
]

export { RoutesArray };
