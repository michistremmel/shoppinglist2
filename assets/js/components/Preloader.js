import React from 'react';

const Preloader = () => {
    return (
        <div className={'preloader-div'}>
            <div className="d-flex justify-content-center">
                <div className="spinner-border" style={{width: '8rem', height: '8rem'}} role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
            </div>
        </div>
    );
};

export default Preloader;