import React from 'react'
import {Link} from "react-router-dom";
import Routing from "routing"
const routes = require('../../../public/js/fos_js_routes.json')
Routing.setRoutingData(routes)

const Header = (props) => {
    return (
        <div className={'col w-100 header p-2'} style={{background: '#1b202d'}}>
            {(props.user !== null && props.user.id)
                ?
                <>
                    <span className={'m-0'}>
                        Eingeloggt als:<br/>
                        <b>{props.user.firstName}</b> | <a href={'/logout'}>Logout</a>
                    </span>
                    <div className={'bootstrap-icon bg-info float-right'}><i className="bi bi-person-fill"></i></div>
                    <Link to={Routing.generate('purchasing')} className={'bootstrap-icon btn-primary mr-1 float-right'}>
                        <i className="bi bi-card-checklist"></i>
                    </Link>
                    <Link to={Routing.generate('shopping_list', {userHash: props.user.shoppingCardHash})} className={'bootstrap-icon btn-secondary mr-1 float-right'}>
                        <i className="bi bi-cart-plus-fill"></i>
                    </Link>
                </>
                :
                <>
                    <a href={'/login'} className={'btn btn-info btn-lg'}>Einloggen</a>
                    <a href={'/register'} className={'btn btn-success btn-lg float-right'}>Registrieren</a>
                </>
            }
        </div>
    );
};

export default Header;
