import React, { useState, useEffect, createRef } from "react"
import {Form, InputGroup, Input, Button} from "reactstrap";
import Routing from "routing"
const routes = require('../../../public/js/fos_js_routes.json')
Routing.setRoutingData(routes)

const Product = (props) => {
    const [preloading, setPreloading] = useState(false)
    const [menu, setMenu] = useState(false)
    const [edit, setEdit] = useState(false)
    const [name, setName] = useState(props.name)

    // Change the Quantity and show Preloader
    const handleClick = (id) => {
        if(props.onClick !== false && !edit){
            props.onClick(id)
            setPreloading(true)
        }
    }

    // If we have the new props.quantity after changing, remove Preloader
    useEffect(() => {
        setPreloading(false)
        setMenu(false)
    }, [props.quantity])

    return(
        <>
            {preloading ?
                <div className={`col-md-12 product border mb-2 ${props.bought && 'bought'}`} style={{height: '52px', paddingTop: '10px'}}>
                    <div className="d-flex justify-content-center">
                        <div className="spinner-border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>
                :
                <div className={`col-md-12 product border mb-2 ${props.bought && 'bought'}`}>
                    <div className={'product-button-div'}>
                        {(!props.bought && props.settings) &&
                        <button className={'btn btn-light float-right mt-1'} onClick={() => setMenu(!menu)}><i className="bi bi-list"></i></button>
                        }

                        {menu &&
                        <>
                            <button className={'btn btn-info float-right mt-1 mr-1'} onClick={() => {props.toggleQuantity(props.id, props.quantity - 1), setPreloading(true)}}><i className="bi bi-dash"></i></button>
                            <button className={'btn btn-info float-right mt-1 mr-1'} onClick={() => {props.toggleQuantity(props.id, props.quantity + 1), setPreloading(true)}}><i className="bi bi-plus"></i></button>
                        </>
                        }

                        {(!edit && props.deleteable) &&
                        <>
                            <button className={'btn btn-danger float-right mt-1'} onClick={() => props.deleteProduct(props.id)}><i className="bi bi-trash"></i></button>
                        </>
                        }

                        {(!edit && props.editable) &&
                        <>
                            <button className={'btn btn-light float-right mt-1 mr-1'} onClick={() => {setEdit(true)}}><i className="bi bi-pencil"></i></button>
                        </>
                        }
                    </div>

                    <h2 onClick={() => handleClick(props.id)}>
                        <span>
                            {props.settings &&
                            <span className={`badge mr-1 bg-${props.bought ? 'dark' : 'info'}`}>{props.quantity}</span>
                            }
                            {
                                edit
                            ?
                                <>
                                    <Form ref={createRef()} onSubmit={(e) => {e.preventDefault(), setEdit(false), props.updateProduct(props.id, name)}}>
                                        <InputGroup className={'mt-2'}>
                                            <Input type={"text"} value={name} onChange={(e) => setName(e.target.value)} />
                                        </InputGroup>
                                        <Button type={'submit'} color={'info'} className={'w-100'}>Speichern</Button>
                                        <Button color={'secondary'} onClick={() => setEdit(false)} className={'w-100 mb-2'}>Abbrechen</Button>
                                    </Form>
                                </>
                            :
                                name
                            }
                        </span>
                    </h2>
                </div>
            }
        </>
    )
}

export default Product;