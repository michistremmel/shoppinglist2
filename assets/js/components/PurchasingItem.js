import React from 'react';
import {Col, Row} from "reactstrap";
import TranslateDate from "../utils/TranslateDate";

const PurchasingItem = (props) => {
    return (
        <Row className={'mb-1 p-1 m-0 product'}>
            <Col xs={12} className={''}>
                {TranslateDate(props.item.timestamp)}
            </Col>
            <Col xs={9} className={''}>
                Lebensmittel
            </Col>
            {/*<Col xs={3} className={''}>*/}
            {/*    {props.item.payment_method}*/}
            {/*</Col>*/}
            <Col xs={3} className={'text-right'}>
                {props.item.total_price}
            </Col>
        </Row>
    );
};

export default PurchasingItem;
