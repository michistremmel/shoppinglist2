import React, {useState, useEffect} from 'react';
import Product from "./Product";
import {Row, FormGroup, Input, Button} from "reactstrap"
import Preloader from "./Preloader";
import Routing from "routing"
const routes = require('../../../public/js/fos_js_routes.json')
Routing.setRoutingData(routes)
import { useParams } from 'react-router-dom';

const ProductList = (props) => {
    const params = useParams()
    const [preloading, setPreloading] = useState(true)
    const [products, setProducts] = useState(null)
    const [productName, setProductName] = useState('')
    const [preloadingSubmitBtn, setPreloadingSubmitBtn] = useState(false)

    const fetchProducts = () => {
        setPreloading(true)
        fetch(Routing.generate('products_index', {userHash: params.userHash}), {method: 'GET'})
            .then(response => response.json())
            .then(data => {
                setProducts(data)
                setPreloading(false)
            })
    }

    const fetchNewProduct = () => {
        fetch(Routing.generate('add_product', {userHash: params.userHash, name: productName}), {method: 'GET'})
            .then(response => response.json())
            .then(data => {
                setProducts(data)
                setProductName('')
                setPreloadingSubmitBtn(false)
            })
    }

    const fetchUpdateProduct = (id, name) => {
        fetch(Routing.generate('update_product', {userHash: params.userHash, id: id, name: name}), {method: 'GET'})
            .then(response => response.json())
            .then(data => {
                fetchProducts()
            })
    }

    const fetchDeleteProduct = (id) => {
        setPreloading(true)
        fetch(Routing.generate('delete_product', {userHash: params.userHash, id: id}), {method: 'GET'})
            .then(response => response.json())
            .then(data => {
                fetchProducts()
            })
    }

    useEffect(() => {
        fetchProducts()
    }, [])

    let productItems = []

    if(products){
        let firstLetter = '';
        Object.keys(products).map((key) => {
            const item = products[key]

            if(!props.productItemsOnCart.includes(parseInt(item.id))){
                if(firstLetter != item.name.charAt(0)){
                    firstLetter = item.name.charAt(0)
                    productItems.push(<h2 className={'text-center mt-3 mb-3'}><b>{firstLetter}</b></h2>)
                }

                productItems.push(
                    <Product
                        key={key}
                        name={item.name}
                        bought={0}
                        id={parseInt(item.id)}
                        quantity={1}
                        onClick={(id) => props.addProductToList(id)}
                        refetchProductlist={() => fetchProducts()}
                        toggleQuantity={false}
                        settings={false}
                        deleteable={true}
                        editable={true}
                        userHash={params.userHash}
                        deleteProduct={(id) => fetchDeleteProduct(id)}
                        updateProduct={(id, name) => fetchUpdateProduct(id, name)}
                    />
                )
            }
        })
    }

    return (
        <>
            {preloading
                ?
                <Preloader />
                :
                <Row>
                    {props.addProduct &&
                        <>
                            <input
                                autoFocus
                                type={'text'}
                                name={'productName'}
                                className={'form-control form-control-lg mb-2'}
                                value={productName}
                                onChange={(event) => setProductName(event.target.value)}
                                placeholder={'Produktname'}
                            />
                            <Button color={'info'} size={'lg'} className={'w-100 mb-4'} onClick={() => {fetchNewProduct(), setPreloadingSubmitBtn(true)}}>
                                {preloadingSubmitBtn
                                    ?
                                    <div className="d-flex justify-content-center">
                                        <div className="spinner-border" role="status">
                                            <span className="visually-hidden">Loading...</span>
                                        </div>
                                    </div>
                                    :
                                    <>Hinzufügen</>
                                }
                            </Button>
                        </>
                    }
                    <p>Klicke auf ein Produkt um es auf die Liste zu packen.</p>
                    {productItems}
                </Row>
            }
        </>
    );
};

export default ProductList;