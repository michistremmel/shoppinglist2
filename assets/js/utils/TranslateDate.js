function TranslateDate(date,option = '',format = '',divider = '.'){
    date = String(date).split(' ');
    let days = '';

    if(date.length == 1){
        days = String(date[0]).split('-');
        return days[2]+divider+days[1]+divider+days[0];
    }
    else if(date.length == 2){
        days = String(date[0]).split('-');
        var hoursTmp = String(date[1]).split('.');
        var hours = String(hoursTmp[0]).split(':');

        if(option == 'noTime'){
            if(format == 'en'){
                return days[0]+divider+days[1]+divider+days[2];
            }
            return days[2]+divider+days[1]+divider+days[0];
        }
        else {
            return days[2]+divider+days[1]+divider+days[0]+' '+hours[0]+':'+hours[1]+':'+hours[2]+' Uhr';
        }
    }
}

export default TranslateDate;