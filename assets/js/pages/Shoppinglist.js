import React, { useState, useEffect } from "react"
import Product from "../components/Product"
import Routing from "routing"
const routes = require('../../../public/js/fos_js_routes.json')
Routing.setRoutingData(routes)
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from "reactstrap"
import Preloader from "../components/Preloader"
import ProductList from "../components/ProductList";
import { useParams } from 'react-router-dom';
import Header from "../components/Header";

const Shoppinglist = () => {
    const params = useParams()
    const [preloading, setPreloading] = useState(true)
    const [user, setUser] = useState([])
    const [products, setProducts] = useState([])
    const [modalAddShoppingCartProduct, setModalAddShoppingCartProduct] = useState(false)
    const [addProduct, setAddProduct] = useState(false)
    const [purchasingTotalPrice, setPurchasingTotalPrice] = useState()

    useEffect(() => {
        fetch(Routing.generate('shopping_list_products', {userHash: params.userHash}), {method: 'GET'})
            .then(response => response.json())
            .then(data => {
                console.log('Shopping Card Products')
                console.log(data)
                setProducts(data.shopping_card)
                setUser(data.user)
                setPreloading(false)

                // if (window.location.hash == "#_=_")
                //     window.location.hash = "";

                if (String(window.location.hash).substring(0,1) == "#") {
                    window.location.hash = "";
                    window.location.href=window.location.href.slice(0, -1);
                }
            })
    }, [])

    const toggleBought = (id = null) => {
        fetch(Routing.generate('shopping_list_change_bought', {userHash: params.userHash, id: id}), {method: 'GET'})
            .then(response => response.json())
            .then(data => {
                setProducts(data)
            })
    }

    const addProductToList = (id = null) => {
        fetch(Routing.generate('shopping_list_add_product', {userHash: params.userHash, id: id}), {method: 'GET'})
            .then(response => response.json())
            .then(data => {
                setProducts(data)
            })
    }

    const toogleQuantity = (id = null, quantity = null) => {
        fetch(Routing.generate('shopping_list_change_quantity', {userHash: params.userHash, id: id, quantity: quantity}), {method: 'GET'})
            .then(response => response.json())
            .then(data => {
                setProducts(data)
            })
    }

    const clearShoppingList = () => {
        setPreloading(true)
        fetch(Routing.generate('shopping_list_delete_products', {userHash: params.userHash}), {method: 'GET'})
            .then(response => response.json())
            .then(data => {
                setProducts(data)
                setPreloading(false)
            })
    }

    const addShoppingCardToPurchasing = () => {
        if (!purchasingTotalPrice) { alert('Rechnungsbetrag eingeben!'); return false }

        setPreloading(true)
        const formData = new FormData();
        formData.append('total_price', purchasingTotalPrice)

        fetch(Routing.generate('add_purchasing'), {method: 'POST', body: formData, headers: {'X-Requested-With': 'XMLHttpRequest'}})
            .then(response => response.json())
            .then(data => {
                setProducts(data)
                setPurchasingTotalPrice(null)
                setPreloading(false)
            })
    }

    const markShoppingCardAsBought = () => {
        return true
    }

    let productItems = []
    let productItemsFinish = []
    let productItemsOnCart = []
    let clearShoppingListBtn = ''

    if(products){
        Object.keys(products).map((key) => {
            const item = products[key]
            productItemsOnCart.push(parseInt(item.product_id))

            if(item.bought){
                productItemsFinish.push(
                    <Product
                        key={key}
                        name={item.name}
                        bought={item.bought === "1" ? 1 : 0}
                        id={parseInt(item.id)}
                        quantity={parseInt(item.quantity)}
                        onClick={(id) => toggleBought(id)}
                        toggleQuantity={(id, quantity) => toogleQuantity(id, quantity)}
                        settings={true}
                    />
                )
            }
            else {
                productItems.push(
                    <Product
                        key={key}
                        name={item.name}
                        bought={item.bought === "1" ? 1 : 0}
                        id={parseInt(item.id)}
                        quantity={parseInt(item.quantity)}
                        onClick={(id) => toggleBought(id)}
                        toggleQuantity={(id, quantity) => toogleQuantity(id, quantity)}
                        settings={true}
                    />
                )
            }
        })

        // Clear ShoppingList Button
        if (productItems.length > 0 || productItemsFinish.length > 0)
            clearShoppingListBtn = <Button color={'danger'} onClick={() => clearShoppingList()} className={'w-100'}>Einkaufsliste leeren</Button>
    }

    return (
        <>
            {preloading
            ?
                <Preloader />
            :
                <>
                    <Header user={user} />
                    <div className={'row m-0 p-2'}>
                        {clearShoppingListBtn}

                        <h1 className={'text-center'}>Zu kaufen</h1>

                        {/*Show Items*/}
                        {products.length > 0 ?
                            <>
                                {productItems.length > 0 ?
                                    <>
                                        <p className={'text-center'}>Klicke auf ein Produkt um es als gekauft zu markieren.</p>
                                        <Button color={'info'} size={'md'} className={'mb-3'} onClick={() => markShoppingCardAsBought()}>Alles als gekauft markieren</Button>
                                        {productItems}
                                    </>
                                    :
                                    <p className={'text-center font-20'}>Fertig, ich bin beeindruckt! :)</p>
                                }

                                <h2 className={'text-center'}>Gekauft</h2>
                                {productItemsFinish.length > 0 ?
                                    <>
                                        <p className={'text-center'}>Klicke auf ein Produkt um es zurück zu setzen.</p>
                                        {user
                                            ?
                                            <>
                                                <input
                                                    autoFocus
                                                    type={'number'}
                                                    name={'purchasingTotalPrice'}
                                                    className={'form-control form-control-lg mb-2 text-center'}
                                                    value={purchasingTotalPrice}
                                                    onChange={(event) => setPurchasingTotalPrice(event.target.value)}
                                                    placeholder={'Rechnungsbetrag'}
                                                />
                                                <Button color={'info'} size={'md'} className={'mb-3'} onClick={() => addShoppingCardToPurchasing()}>Einkauf ins Kassenbuch buchen</Button>
                                            </>
                                            :
                                            <a href={Routing.generate('app_login')} className={'btn btn-info btn-md mb-3'}>Anmelden zum buchen ins Kassenbuch</a>
                                        }
                                        {productItemsFinish}
                                    </>
                                    :
                                    <p className={'text-center font-20'}>Ich glaube, Du hast noch was zu tun, oder?!</p>
                                }
                            </>
                            :
                            <p className={'text-center font-20'}>
                                Wird mal Zeit was einzukaufen!<br /><br />Klicke unten auf das "+" um Produkte auf Deine Liste zu packen und/oder neue Produkte anzulegen.
                            </p>
                        }

                        <button className={'btn btn-info btn-lg add-product shadow'} onClick={() => setModalAddShoppingCartProduct(true)}><i className="bi bi-plus"></i></button>

                        <Modal
                            backdrop={false}
                            centered
                            fullscreen
                            isOpen={modalAddShoppingCartProduct}
                        >
                            <ModalHeader>
                                Deine Produkte
                                <Button color={addProduct ? 'success' : 'info'} className={'float-right'} onClick={() => setAddProduct(!addProduct)}>{addProduct ? 'Fertig' : 'Neues Produkt'}</Button>
                            </ModalHeader>
                            <ModalBody>
                                {modalAddShoppingCartProduct &&
                                    <ProductList
                                        productItemsOnCart={productItemsOnCart}
                                        addProductToList={(id) => addProductToList(id)}
                                        addProduct={addProduct}
                                    />
                                }
                            </ModalBody>
                            <ModalFooter>
                                <Button color={'info'} className={'w-100'} onClick={() => setModalAddShoppingCartProduct(false)}>
                                    Fertig!
                                </Button>
                            </ModalFooter>
                        </Modal>
                    </div>
                </>
            }
        </>
    )
}

export default Shoppinglist;