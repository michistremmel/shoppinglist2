import React, { useState, useEffect } from "react"
import Product from "../components/Product";

const Products = () => {
    return(
        <div className={'row m-0 p-2'}>
            <h1 className={'text-center'}>Einkaufsliste</h1>
            <Product name={'Product 1'} />
            <Product name={'Product 2'} />
        </div>
    )
}

export default Products;