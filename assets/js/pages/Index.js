import React, { Component } from 'react';
import Shoppinglist from "./Shoppinglist";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import Routing from "routing"
const routes = require('../../../public/js/fos_js_routes.json')
Routing.setRoutingData(routes)

import Products from "./Products";
import Purchasing from "./Purchasing";

class Index extends Component {
    constructor(props){
        super(props)
    }
    
    render() {
        return (
            <BrowserRouter>
                <Routes>
                    <Route exact path="/shoppinglist/:userHash" element={<Shoppinglist />} />
                    <Route exact path="/products" element={<Products />} />
                    <Route exact path={Routing.generate('purchasing')} element={<Purchasing />} />
                </Routes>
            </BrowserRouter>
        )
    }
}

export default Index;