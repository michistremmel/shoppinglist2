import React, { useEffect, useState } from 'react';
import Routing from "routing"
const routes = require('../../../public/js/fos_js_routes.json')
Routing.setRoutingData(routes)
import { Row, Col } from "reactstrap"
import Preloader from "../components/Preloader"
import Header from "../components/Header";
import PurchasingItem from "../components/PurchasingItem";
import { FormGroup, Input } from "reactstrap";

const Purchasing = () => {
    const [preloading, setPreloading] = useState(true)
    const [purchasing, setPurchasing] = useState([])
    const [user, setUser] = useState([])
    const [purchasingUserId, setPurchasingUserId] = useState([])
    const [userList, setUserList] = useState([])

    useEffect(() => {
        fetch(Routing.generate('get_purchasing'), {method: 'GET'})
            .then(response => response.json())
            .then(data => {
                setPurchasing(data.purchasing)
                setUser(data.user)
                setPurchasingUserId(data.user.id)
                setUserList(data.user_list)
                setPreloading(false)
                console.log(data)
            })
    }, [])

    let totalSumm = 0
    let totalSummPurchasingUserId = 0
    let purchasingRows = []
    if(purchasing.length){
        purchasing.map((item, index) => {
            totalSumm = totalSumm + parseFloat(item.total_price)

            if(purchasingUserId && item.user_id.indexOf(purchasingUserId) === -1){
                return;
            }

            totalSummPurchasingUserId = totalSummPurchasingUserId + parseFloat(item.total_price)

            purchasingRows.push(
                <PurchasingItem key={index} item={item} />
            )
        })
    }

    let purchasingUserOptions = []
    console.log(userList)
    if(userList){
        Object.keys(userList).map(index => {
            purchasingUserOptions.push(
                <option key={index} value={index}>{userList[index]}</option>
            )
        })
    }

    return (
        <>
            {preloading
                ?
                <Preloader/>
                :
                <>
                    <Header user={user}/>
                    <Row className={'m-0 p-2 pt-0'}>
                        <Col xs={12} className={'text-center p-0 pt-2'}>
                            <h1>Meine Ausgaben</h1>
                            <p>
                                <b>Ausgaben alle Benutzer:</b> {totalSumm.toFixed(2).replace('.', ',')} €<br />
                                <b>Ausgaben aktueller Benutzer:</b> {totalSummPurchasingUserId.toFixed(2).replace('.', ',')} €
                            </p>
                            <FormGroup>
                                <Input type={'select'} name={'purchasingUserId'} onChange={(e) => setPurchasingUserId(e.target.value)}>
                                    {purchasingUserOptions}
                                </Input>
                            </FormGroup>
                        </Col>
                        {purchasingRows}
                    </Row>
                </>
            }
        </>
    );
};

export default Purchasing;
