import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import '../styles/app.css';
import Index from "./pages/Index";

ReactDOM.render(
    <Index />, document.getElementById('root')
);